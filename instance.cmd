###############################################################################

epicsEnvSet("CONTROL_GROUP",  "PBI-BPM02")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-110")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-6")
epicsEnvSet("EVR_NAME",       "PBI-BPM02:Ctrl-EVR-201:")

epicsEnvSet("SYSTEM1_PREFIX", "SPK:PBI-BPM-001:")
epicsEnvSet("SYSTEM1_NAME",   "SPK BPM 01")

epicsEnvSet("SYSTEM2_PREFIX", "SPK:PBI-BPM-002:")
epicsEnvSet("SYSTEM2_NAME",   "SPK BPM 02")

epicsEnvSet("IOCNAME", "PBI-BPM02:SC-IOC-204")
epicsEnvSet("IOCDIR", "PBI-BPM02_SC-IOC-204")

###############################################################################
